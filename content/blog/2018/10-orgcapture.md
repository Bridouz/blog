+++
title = "Org-Capture, la prise de note rapide"
date = 2018-10-08T10:00:00+01:00
[taxonomies]
tags = ["Emacs", "Orgmode"]
+++

Dans mon exploration d'**emacs** et org-mode je suis tombé sur _org-capture_. Un système de prise de notes ultra rapide avec des modèles pouvant être prédéfinis. Pratique lorsque l'on travaille sur un document et qu'une idée surgit sans crier gare ou qu'une tâche à accomplir vient de faire irruption dans votre cerveau.

![Org capture Pop](/img/emacs/orgcapturepop.png)

```
(require 'org-capture)
(require 'org-protocol)
(require 'org-capture-pop-frame)
(global-set-key (kbd "C-c c") 'org-capture)
```

Ce qui est génial c'est qu'il est possible de programmer des _templates_ pour différentes prises de notes. Ainsi à l'invocation d'`org-capture` un choix est demandé pour utiliser tel ou tel template en fonction de la note.

```
(setq org-capture-templates
  '(
    ("n" "Note"
	 entry (file+headline "~/org/StuffBox.org" "Notes")
	 "** %^{Title} %^g \n%?\n")
    ("l" "Web Link"
	 entry (file+headline "~/org/StuffBox.org" "Web")
	"** %a %^g \n" :immediate-finish t)
    ("p" "Web Paste"
	 entry (file+headline "~/org/StuffBox.org" "Web")
	 "** %a %^g \n %i\n" :immediate-finish t)
   )
)
```

**Org-Capture** sert donc à prendre des notes rapidement ou enregistrer des informations pour plus tard. Et quoi de mieux comme lieu de découverte qu'un navigateur web (ouvert sur les bonnes pages bien entendu) ?<br />
J'utilise _Firefox_ et des [extension existent](https://addons.mozilla.org/fr/firefox/search/?platform=linux&q=org-capture) pour intégrer `org-capture` au navigateur, elles fonctionnent bien mais ne sont pas très fléxibles. Je préfère alors utiliser un bookmarlet en `javascript` pour communiquer avec org-capture via `org-protocol`. La solution permet de sauvegarder un simple lien, mais peut également sauvegarder le texte mis en surbrillance sur la dite-page. Vraiment sympa pour relever des citations, des phrases ou morceaux de code.

```javascript
javascript:location.href="org-protocol:///capture?url="+encodeURIComponent(location.href)+"&title="+encodeURIComponent(document.title||"[untitled page]")+"&body="+encodeURIComponent(window.getSelection())
```

_Bien naturellement un bookmarlet JavaScript est utilisé ici, donc la solution peut être appliquée avec les autres navigateurs._

L'intégration dans Firefox c'est bien, mais il est possible d'aller encore plus loin en créeant un raccourcis clavier dans votre système pour pouvoir avoir accès à `org-capture` depuis n'importe où.

```bash
emacsclient org-protocol://capture?
```

En invoquant cette commande je fais le choix délibéré de ne pas choisir de templates particulié, car il est possible que je veuille simplement entrer un note ou alors ajouter une tâche ou je ne sais quoi encore.

Mais alors il me manque une chose pour adapter mon usage à cette technologie : _Du popup_. Je ne souhaite pas passer par une instance d'emacs et sortir de mon logiciel utilisé. J'aime l'idée qu'une fenêtre surgisse, permette une prise de notes rapide, une sauvegarde de liens puis se referme en me laissant avec mon logiciel ouvert.

La solution `org-capture-pop-frame`, un paquet qui comme son nom l'indique transforme chaque appelle d'`org-capture` en une fenêtre popup. Avec le paramètre `immediate-finish t` c'est un vrai plaisir.

```
(defadvice org-capture-finalize
  (after delete-capture-frame activate)
  "Advise capture-finalize to close the frame"
  (if (equal "capture" (frame-parameter nil 'name))
  (delete-frame)))

(defadvice org-capture-destroy
  (after delete-capture-frame activate)
  "Advise capture-destroy to close the frame"
  (if (equal "capture" (frame-parameter nil 'name))
  (delete-frame)))

(use-package noflet
  :ensure t )
(defun make-capture-frame ()
  "Create a new frame and run org-capture."
  (interactive)
  (make-frame '((name . "capture")))
  (select-frame-by-name "capture")
  (delete-other-windows)
  (noflet ((switch-to-buffer-other-window (buf) (switch-to-buffer buf)))
  (org-capture)))
```
![Org capture Firefox](/img/emacs/orgffcapture.png)
