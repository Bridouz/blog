+++
title = "Kodi et Rétrogaming"
date = 2018-08-31T09:30:00+08:00
[taxonomies]
tags = ["Raspberry", "Jeux"]
+++

Il y a quelques jours j'ai vu passé l'info sur le net comme quoi [Nintendo avait lancé des poursuites](https://torrentfreak.com/why-nintendo-will-never-defeat-retro-gamers-180912/) contre un grand nombre de sites de *retro-gaming*. Résultat: des fermetures à la pelle.  
Bien évidement cela m'a donné envie de rejouer à ces jeux de mon enfance.

Le *rétro-gaming* est un phénomène *nostalgique* de grande ampleur, nous ramenant à une époque où les jeux avait une certaine saveur et, où, le marché hollywoodien n'avait pas encore amené cette notion de *grand spectacle*. Pour beaucoup il est lié à des souvenirs d'enfance, des moments partagés devant la console. Pour ma part c'est toujours un plaisir de rejouer à ces jeux, de me rappeler ces moments avec mon frère, mon père. Ces parties sans fin, ces nuits à essayer d'avancer dans *Le Roi Lion* sur **Snes** jusqu'à ce que ma mère rentre du boulot pour nous trouver encore éveiller. (Le père a dû se faire légerement engueulé cette fois ci.)

Bref, ce plaisir (le plus souvent pas tout à fait légal) est en train de disparaitre avec les attaques récentes de Nintendo. En y réfléchissant c'est à la fois logique et incompréhensible.

  - **Logique** car Nintendo, et les autres éditeurs, ont bien compris ce phénomène et commence à surfer dessus avec les réeditions de leurs vieilles consoles comprenant les hits de l'époque. Ça ne leur coute pas grand-chose et ça rapporte un max.
  - **Incompréhensible**, car ces consoles et ces jeux ont fait leur temps, ont rapporté beaucoup d'argent et ont permis d'assurer la rentabilité financière et, parfois, le respect créatif des éditeurs. Pourquoi alors venir embetter ces sites et joueurs voulant simplement revivre des moments de plaisirs ? Tout en sachant qu'un grand nombre de ces rétro-gamers jouent toujours, et ce sur des consoles récentes en achetant légalement les jeux ?

Quoi qu'il en soit j'ai eu envie de m'y remettre un peu (hier soir d'ailleurs nous avons, avec Madame, refait **Donkey Kong Country** sur *Snes*. Un réel plaisir.) et j'en profite d'avoir un *Raspberry Pi* pour installer **Recalbox**.

Dans le monde des distributions multi-émulation il y a deux grands noms:

  - Recalbox
  - Retropie

J'ai voulu essayer *Recalbox* car elle apparaît comme étant la solution la plus facile d'accès pour l'utilisateur. Un simple plug-n-play et tout est sensé fonctionner sans besoins d'aller bidouiller quoique ce soit.  
Effectivement c'est le cas, tout est reconnu sans le moindre effort. Les émulateurs (Megadrive, Nes et Snes) fonctionnent à merveille. Un pur plaisir pour la partie rétro-gaming. Cependant *Recalbox* propose également *Kodi* pour faire office de media-center et, la version proposée est plus qu'obsolète pour l'utiliser avec des addons.


Alors je suis parti sur l'idée d'avoir un dual-boot sur mon Raspberry pour pouvoir profiter d'un Kodi dernier cri et de recalbox. Pour cela j'ai utilisé [NOOBS](https://www.raspberrypi.org/downloads/noobs/) puis [PINN](https://github.com/procount/pinn/).

  - LibreElec/Recalbox -> Installation fonctionnelle pour LibreElec, Recalbox n'accroche pas au boot. Problème connu, sans solution vraisemblablement.
  - OSMC/Recalbox - Les deux démarrent sans problème, j'ai cependant du mal avec l'interface maison. Il est possible de repasser sur le thème de base de *Kodi* mais, je ne sais pas pourquoi je préfère LibreElec dans sa simplicité.

Finalement c'est moyennement satisfaisant ces dual-boots. Alors j'ai cherché, me suis rappelé qu'il y avait un add-on pour utiliser **RetroArch** ou **EmuleStation** avec *LibreElec*, [GameStarter](https://github.com/bite-your-idols/Gamestarter), qui faisait super bien le job. Et puis j'ai découvert via les résultats du moteur de recherche que Kodi, dans sa dernière version *(18)* en cours de développement, avait intégré une partie nommée **RetroPlayer**.

Ainsi Kodi s'étoffe encore en apportant toute la joie du retrogaming. J'ai ainsi commencé à explorer la chose en flashant l'image alpha de LibreElec 9 avec Kodi 18. Un menu **Jeux** est présent, les émulateurs sont disponibles à l'installation dans le dépôt LibreElec et il suffit alors de récupérer des Roms.  
Lancement ... Et ça fonctionne, enfin pas tout car certains émulateurs ne fonctionnent pas vraiment mais pour la SNES et la Mega-drive on en a au moins un pour chacune des consoles. Les manettes (Xbox 360 wireless et  une imitation 360 filaire) sont reconnus sans besoin de bidouiller.

Très bonne surprise donc si vous cherchez une solution simple pour un combo média-center/rétro-gaming. Kodi gère le tout et, même si la deuxième partie n'est pas encore complète (système de sauvegarde non implanté) cela est très prométteur. Surtout pour une solution qui, originellemetn, a vu le jour sur une console de jeu (Xbox).

Si l'aventure vous tente c'est par ici:

  - [Image LibreElec 8.90.003](https://libreelec.tv/2018/08/libreelec-leia-v8-90-003-alpha/)
  - [Builds journaliers de la branche 9 par Milhouse](https://forum.kodi.tv/showthread.php?tid=298461)
