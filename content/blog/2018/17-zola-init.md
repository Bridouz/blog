+++
title = "Zola init"
date = 2018-12-04T20:20:00+01:00
[taxonomies]
tags = ["Zola"]
+++

Il le fallait, c'était même incontournable. Après des semaines de bons et loyaux services j'ai viré **Hugo** pour générer ce blog et l'ai remplacé par **Zola**. Pas de grands changements, plutôt une simplification du procédé, un changement au niveau du générateur pour plus de clareté.

## Gutenberg -> Zola

Le nom initiale du générateur était **Gutenberg**, mais avec le déploiement de l'éditeur du même nom par *Wordpress* il.ne restait que peu de place sur le.net pour que le générateur se fasse un nom et puisse être visible.

C'est donc pour cela que Vincent Prouillon (aka [Keats](https://github.com/Keats)) a pris la décision de changer le nom pour **Zola**.

Pour le moment les fichiers binaires ne sont disponibles uniquement pour les architectures `x86_64`. J'ai compilé les sources sur mon *Raspberry Pi* en utilisant [Rustup](https://rustup.rs/) pour avoir la dernière version du language. Ça a pris 80 minutes et j'ai désormais un binaire pour les architectures `arm` (hormis Android).


## le templating

Une des choses qui me repoussait avec **Hugo** était surtout son moteur de template absolument imbitable. La syntaxe, l'affichage d'erreurs, je n'y comprenais rien et il s'avérait relativement complexe à prendre en main, très implicite trop même. Bien qu'une fois le thème et son fonctionnement en place normalement il n'y avait plus de soucis j'aime avoir un mécanisme clair, histoire de savoir où je suis.

**Zola** possède son propre moteur nommé *[Tera](https://tera.netlify.com/)* et écrit par le même developeur. Il s'inspire de moteurs tels *Jinja2*. Et effectivement je m'y retrouve plus avec cette syntaxe propre et concise. Et quoi de mieux pour avoir un blog relativement simple qu'un moteur qui l'est tout autant. Pour le moment j'ai réussi à réecrire tout ce que j'utilisais avec *Hugo* pour *Zola*, et cela en moins d'une heure. Preuve s'il en est de la simplicité de comprehesion du moteur pour un cerveau simple comme le mien.


## Les fonctionnalités

**Zola** est écrit en *Rust* et donc bénéficie d'un language compilé permettant de faire tenir le tout dans un fichier binaire unique. Contrairement à des générateurs comme *Pelican* ou *Jekyll* respectivement écrits en *Python* et *Ruby*. Pas de soucis à se faire pour les librairies utilisées donc.

Les fonctionnalités proposées reprennent en gros l'offre que propose tout bon générateur de site statique.

  -   Markdown pour le contenu
    -   Utilisation de shortcodes
  -   Support des *catégories* et *tags*
  -   Flux RSS
  -   Fichier de configuration en `toml`
  -   *LiveReload* avec `zola serve` pour prévisualiser les changements en temps réel
  -   Colorisation syntaxique
  -   Redimenssionnement des images

De part son écriture en `rust` la génération se fait en très peu de temps. Avec le contenu actuel le blog est généré en 110ms.


## Simplification globale

Je trouve réellement que **Zola** semble plus simple à prendre en main qu'*Hugo*. Pour un modeste blog il y a tout ce qu'il faut et c'est un plaisir à utiliser. De plus je comprends beaucoup plus son aspect *templating* et je peux me débrouiller quasiment seul tant la syntaxe est claire. Les erreurs sont également plus rapidement détectable, l'interface CLI indiquant la potentielle source d'erreur.

Là où j'aime ce genre de logiciels c'est qu'une fois le tout mis en place je n'ai quasiment plus à le lancer en local pour vérifier que tout fonctionne, un simple *push* avec `git` sur le serveur, un `git-hook` qui lance la génération et le contenu est directement disponible sur le blog. C'est ça en fait un bon moteur de blog, un outil qui sait faire oublier ses rouages pour permettre de se concentrer sur le plus important, le contenu, l'écriture.

Les `SSG` ont le vent en poupe depuis quelques temps. Il y en a écrit dans tout un tas de langages, l'ami [Thuban](https://yeuxdelibad.net) en écrit même un, [SWX](https://3hg.fr/Scripts/swx/), tout simple uniquement à base de scripts `bash`. Pour ma part je ne suis pas encore prêt à passer à un générateur aussi simple. J'aime pouvoir appliquer des tags à mes billets, pouvoir redimensionner des images à la génération. Qui sait un jour&#x2026;

En tout cas **Zola** est un générateur vraiment intéressant, relativement simple à prendre en main. J'espére qu'il saura garder cette simplicité d'utilisation, quite à ne pas fournir une palette de fonctionnalités à n'en plus finir.

<https://www.getzola.org>

