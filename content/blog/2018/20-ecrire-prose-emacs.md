+++
title = "Écrire de la prose avec Emacs"
date = 2018-12-23T09:00:00+01:00
[taxonomies]
tags = ["Emacs", "Écriture"]
+++

N'étant pas un développeur/codeur/admintruc je n'utilise que très rarement **emacs** pour écrire du code. J'écris principalement de la prose et j'aime avoir un environnement d'écriture propre et complet.

Voici donc ce que j'utilise pour écrire.

J'ai dans un premier temps essayer `vim` que j'ai très rapidement abandonné car bien que léger, je ne le trouvais pas intuitif pour l'écriture et il me manquait deux ou trois fonctionnalités par rapport à `emacs`. L'édition modale peut avoir son intérêt et je pense que je reconsidérerai la question une fois vraiment à l'aise avec `emacs` en général en utilisant `evil`.


## Thème

Le but ultime de l'écriture est de se créer un environnement confortable. Pour ma part cela inclue une minimum de distraction et c'est pourquoi j'ai opté pour le thème [Poet](https://github.com/kunalb/poet) où l'auteur l'a pensé tout particulièrement pour l'écriture.

Un fond légèrement coloré, une charte graphique sobre tout en sachant mettre en relief les blocs de codes ou de citations. C'est tout ce dont j'ai besoin.


## Interface

Je pourrais diviser mes phases d'écriture en deux catégories :

  -   La prise de note
  -   l'écriture

La première ne nécessite pas de réglage particulier hormis la fonction popup avec `org-capture` dont j'avais parlé il y a quelques jours. Sinon quand je travaille sur une formation avec un support pdf ou diapo je partage souvent mon écran en deux fenêtres pour lire et prendre des notes en même temps.

En revanche lorsque j'écris un billet j'aime avoir **emacs** en plein écran. Le plus souvent j'écris en utilisant *org-mode* et donc mes parties sont structurés avec des headers. Le fichier comprenant les brouillons du bloc compte un billet par header de niveau 1. Parfois il m'arrive d'éditer plusieurs billets simultanément ou de vouloir me concentrer uniquement sur une partie précise du billet. Alors je split la fenêtre emacs en deux, avec un ratio 1/3.

La partie de gauche n'affiche que les headers, un peu comme une table des matières et celle de droite affiche le header où j'écris actuellement.

Pour cela j'utilise deux raccourcis maison et une fonction glissant automatiquement le focus sur la `frame` nouvellement créée.

```emacs-lisp
("C-M-q" . org-tree-open-in-right-frame)
("C-M-z" . org-tree-to-indirect-buffer)

(defun org-tree-open-in-right-frame ()
  (interactive)
  (org-tree-to-indirect-buffer)
  (windmove-right))
```
![Org mode Layout](/img/emacs/orgmodelayout.png)

## Orthographe

Pour la vérification orthographique j'utilise `flyspell` avec le dictionnaire `hunspell`. Là encore, et c'est bien le charme d'emacs, j'utilise deux fonctions pour l'utilisation des dictionnaires.

```emacs-lisp
(setq ispell-program-name (executable-find "hunspell")
      ispell-dictionary "fr_FR")

(bind-key "C-c F"
          (lambda ()
            (interactive)
            (ispell-change-dictionary "fr_FR")
            (flyspell-buffer)))

(bind-key "C-c E"
          (lambda ()
            (interactive)
            (ispell-change-dictionary "en_GB")
            (flyspell-buffer)))

(use-package flyspell-correct-ivy
  :ensure t
  :after flyspell
  :bind (:map flyspell-mode-map
              ("C-;" . flyspell-correct-word-generic)))
```

Le plus souvent une fois qu'une session d'écriture touche à sa fin et qu'il faut alors réviser le texte. Je me concentre sur l'ortographe et avec les raccourcis `C-c F` et `C-c E` je peux rapidement activer la correction selon la langue souhaitée. Quant à `C-;`, le raccourcis permet de corriger un mot via une liste de suggestions présents dans le minibuffer via `ivy`

![Flyspell et Ivy](/img/emacs/flyspell_ivy.png)

## Grammaire

Pour la grammaire franchouillarde c'est tout naturellement que j'utilise l'excellent [Grammalecte](https://grammalecte.net/) avec `flycheck-grammalecte`. Le plus souvent je ne le lance pas au démarrage et préfère attendre la fin d'une session d'écriture pour l'activer et corriger si besoin.
