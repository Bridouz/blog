+++
title = "Org-Mode, finalement"
date = 2018-09-21T17:30:00Z
[taxonomies]
tags = ["Emacs", "Linux", "Orgmode"]
+++

Il y a quelques semaines je disais avoir essayé **Org-Mode** et l'avoir trouvé bien trop complet et préférais ainsi continuer à utiliser **Markdown** pour la mise en forme de mes billets et mes notes. C'est donc tout naturellement que j'ai relancé _Emacs_ la semaine dernière...<br />

La raison est simple, je suis actuellement une formation professionnelle et je voulais un outil pour retaper mes notes manuscrites, un outil simple et me permettant de faire des recherches dedans plutôt rapidement. La formation étant articulée autours de quatre modules le système d'_headings_ avec possibilité de pliage et le réarrangement facilité de ces derniers me paraît être une fonctionnalité intéressante pour la mise en forme de mes notes.w

Org-Mode donc, quelques vidéos (pour confirmer mon choix) ici et là et me voilà parti.

Et bien évidemment, je m'y suis plongé et je pense avoir trouvé un environnement d'écriture qui me convient. La syntaxe est similaire à **Markdown** et s'appréhende rapidement.

Ce qui fait la force d'**Org-Mode** c'est ses fonctionnalités à la pelle.

Parmi celles-ci je me dois de parler d'_org-capture_. Cette dernière permet de rapidement écrire de petites notes ou encore de sauvegarder une information importante comme un lien URL, un lien avec un morceau de texte issu de la page web, etc.<br />
Quelques lignes de code plus tard les templates sont générés pour différentier les divers usages et il est même possible d'invoquer la fonction depuis un navigateur ou le système. Très pratique lorsqu'une idée survient de nulle part et qu'il faut la coucher sur papier numérique rapidement. j'y reviendrai dans un billet prochain tant la fonction est utile et pratique.

> Everything should be made as simple as possible, but not any simpler -- Albert Einstein

**Org-Mode** propose également l'export vers un grand nombre de format (HTML, Markdown, ODT, Latex ou encore PDF) et là aussi il est possible de choisir d'exporter un _heading_ en particulier ou l'intégralité du fichier.

![Orgmode Layout](/img/emacs/orgmodelayout.png)

Je crois qu'à l'heure actuelle je n'ai exploré qu'une faible partie des diverses possibilités qu'offre **Org-Mode** mais je sens déjà que l'outil me plaît. Certes l'apprentissage est rude au début, car il faut assimiler une nouvelle syntaxe, un concept d'headings et subheadings mais, je pense qu'il ne faut pas s'y plonger à corps perdu dès le début et d'accepter de ne pas tout maîtriser puis, apprendre petit à petit à maitriser les raccourcis et les diverses fonctionnalités pour se construire une méthode de travail propre et efficace.
