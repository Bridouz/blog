+++
title = "Subliminal, sous-titres et Nautilus"
date = 2018-08-14T16:00:00+02:00
[taxonomies]
tags = ["Linux", "Scripts", "Outils"]
+++

Il arrive parfois qu'en *enregistrant de vidéos à la télévision sur votre enregistreur VHS* (modernisé la méthode si besoin) vous ayez des vidéos dans un autre langage que celui que vous parlez. Bien évidemment il est possible de récupérer des sous-titres sur internet. Et quand vous pouvez le faire directement depuis votre gestionnaire de fichiers c'est encore mieux.

Utilisant *Budgie* et donc le stack *GNOME* je suis parti à la recherche une solution utilisable avec Nautilus. J'ai trouvé deux scripts répondant à mon besoin.

Le [premier](https://github.com/emericg/OpenSubtitlesDownload), *OpenSubtitlesDownload*, comme son nom l'indique, cherchera uniquement sur OpenSubtitles.

Le [deuxième](https://github.com/Diaoul/nautilus-subliminal), *Nautilus-Subliminal* référence plusieurs sites de sous-titres dont le précédent et Addic7ed. L'interface permet de choisir la langue voulue, de régler deux trois choses.

Le script d'installation part du principe que vous avez une distribution fonctionnant avec le gestionnaire de paquets *apt*. Vu que j'utilise **Solus** et son *eopkg* ce n'est pas le cas, il est tout de même possible de l'installer manuellement en s'inspirant du script de base ou alors un simple coup d'oeil aux *pull requests* en attente permet de voir qu'un utilisateur [a déjà porté](https://github.com/Diaoul/nautilus-subliminal/pull/7/commits/5a6a7c9286e9e196deb36ff2b360edcebebc73a0) le dit-script pour **Solus**.

Une fois installé il faudra redémarrer la session pour pouvoir avoir accès à **Subliminal** au clic droit sur un fichier vidéo.  
L'outil est vraiment agréable à utiliser et avec les sources de sous-titres disponibles il sera difficile de le mettre à l'amende. De plus il est possible de sélectionner toutes les vidéos présentes dans un dossier et **Subliminal** ira chercher chaque sous-titre tout seul, en le renommant pour correspondre au nom de votre fichier vidéo. Sympa non.

Bonne soirée magnétoscope. ;)
