+++
title = "Org-Mode et configuration d'emacs"
date = 2018-11-21T17:00:00+01:00
[taxonomies]
tags = ["Emacs", "Orgmode"]
+++

Emacs est un logiciel utilisant le langage de programmation `lisp` et tout fichier servant à le configurer est écrit en `.el` bien naturellement, mais pas seulement.

Ce qui est génial avec emacs c'est **Org mode**, outil à tout faire, et il est possible décrire son fichier de configuration général (_init_) en utilisant Org mode pour ainsi bénéficier de tous ses avantages de mise en forme.

On appelle cela de la _programmation lettrée_ et je dois bien avouer que pour un non-programmeur c'est une chose absolument géniale. Non seulement _Emacs_ se veut être une machine personnalisable à souhait mais la possibilité de le faire en détaillant en prose absolument tout ce que l'on veut est génial pour s'y repérer et pouvoir rapidement comprendre à quoi serve les lignes de codes ajoutées.

Ces dernières doivent être contenues dans un bloc `SRC` (rapidement mis en forme avec `<s` auquel il suffit de rajouter la langue `emacs-lisp`.

```
#+TITLE: Emacs config
#+AUTHOR: Justin Bridouz
#+PROPERTY: header-args
#+STARTUP: overview

* Package
** Set package repositories

#+BEGIN_SRC emacs-lisp
  (package-initialize)

  (add-to-list 'package-archives
	       '("melpa" . "https://melpa.org/packages/"))

  (when (not package-archive-contents)
    (package-refresh-contents))
#+END_SRC
```

Avec **Org-Mode** il est possible de structurer son document avec des en-têtes `*`, pratique pour organiser son fichier de configuration. Le mien par exemple est structuré de la sorte.

pour cela j'ai créé un fichier `config.org` dans le dossier `.emacs.d` avec ma config structurée avec _Org mode_ puis mon fichier `init.el` contient :

```
(org-babel-load-file
(expand-file-name "config.org" user-emacs-directory))
```
