+++
title = "MTP simplement en ligne de commande"
date = 2019-01-30T09:00:00+01:00
[taxonomies]
tags = ["Android", "CLI"]
+++

**MTP** c'est une bien belle connerie, un moyen de supprimer les transferts classiques via _usb_ par un protocole verrouillé et franchement pas pratique.

Fort heureusement il est possible de l'utiliser plus ou moins convenablement avec un gestionnaire de fichier graphique (_Gnome_ ou _Xfce_ le supportent très bien, _Kde_ depuis très récemment). En revanche en ligne de commande c'est autre chose, et c'est là qu'intervient `simple-mtpfs`.

> Pensez bien à utiliser [le fichier udev 
global](https://github.com/M0Rf30/android-udev-rules/blob/master/51-android.rules) répertoriant toutes les propriétés de montage des différents appareils 
connus pour utiliser **MTP**. Sans ce fichier, il est possible que le 
montage ne se fasse pas, surtout si vous souhaitez bidouiller de 
l'auto montage.

Le programme est écrit en `c++` et ses sources sont trouvables sur [Github](https://github.com/phatina/simple-mtpfs). Le logiciel n'a pas vu un _commit_ depuis fin 2016, mais il fonctionne toujours aussi bien, et cela sans problèmes relevés pour ma part. Le paquet est bien entendu disponible sur un bon nombres de distributions et peut se compiler très facilement sachant qu'il ne dépend que de `fuse >=2.7.3` et `libmtp`.

Une fois le logiciel installé il est possible d'identifier les appareils 
connectés à votre ordinateur avec `simple-mtpfs -l`. Le logiciel permet 
de monter un appareil dans votre répertoire `$HOME`, par exemple j'ai 
créé un dossier `AndroidDevice` et je monte mon smartphone dessus 
lorsque j'ai besoin de l'utiliser. Pas de `sudo` ni d'emplacements 
compliqués à atteindre sans permissions.   
Par la suite deux solutions s'offrent à vous :

  - Vous avez **plusieurs** éléments connectés et il est alors possible 
de choisir lequel vous voulez monter avec `simple-mtpfs --device 
<numéro_de_l'appareil> ~/AndroidDevice1`
  - Vous n'avez qu'**un seul** appareil connecté et un simple 
`simple-mtpfs ~/AndroidDevice` fera l'affaire.

Une fois que vous voulez démonter l'appareil `fusermount -u ~/AndroidDevice` sera la commande à utiliser. Le procédé est donc relativement simple finalement, d'où peut-être le nom du logiciel.
