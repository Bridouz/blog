+++
title = "Mystery Box et Nextcloud"
date = 2019-04-05T10:00:00+01:00
[taxonomies]
tags = ["Outils"]
[extra]
song = "[Riverside - Out of Myself](https://www.youtube.com/watch?v=LXL_7s7T79w)"
+++

J'avais vu passer le concept il y a quelques temps, m'étais dit qu'il serait chouette de mettre ça en place puis, comme _trop_ souvent, mon cerveau a mis l'info de côté et est passé à autre chose.

Bref, c'est enfin chose faite. La `Secret Box` est là. Comment ça se boutique me direz-vous, eh bien c'est très simple.

J'utilise Nextcloud où je crée un dossier et je partage son url en spécifiant qu'il s'agit d'un partage par envoi, sans possibilité de lister les fichiers présents dans le dossier.

![Nextcloud Mystery Box](/img/solus/mysterybox_sharerenamer.png)

Rien de plus simple, et l'url qui suit vois permettra de déposer ce que vous voulez. Si vous souhaitez vous aussi avoir une url de partage propre et personnalisable il fait installer l'application [ShareRenamer](https://apps.nextcloud.com/apps/sharerenamer).

<https://cloud.bloguslibrus.fr/s/mystery-box>

J'aime ce concept et cela me rappelle la _mystery box_ de **JJ Abrams**. Concept qu'il expliqua dans une [conf TedX](https://www.ted.com/talks/j_j_abrams_mystery_box) et qui venait apporter des informations sur sa façon d'écrir des histoires. Selon lui le but étant de créer une zone d'ombre dans le scénario, attirant le lecteur à se poser des questions et ne jamais y répondre.

Dans une société où on se doit de tout connaître, tout le temps, je trouve le procédé plus que salvateur. Bien sûr certains y trouveront ici un moyen peu travaillé de combler un manque scénaristique dans une histoire. Au contraire je trouve que cette **mystery box** permet de mettre en action l'imaginaire, la réflexion et le débat. Il est intéressant de noter que chaque individu peut avoir sa propre version du mystère, chaque idée étant plausible.

La magie de la culture réside, en partie selon bibi, dans cette part de mystère insufflée dans une œuvre. Tout comprendre sans se creuser un peu la tête c'est une solution bien trop simpliste pour les humains que nous sommes.  
Si l'aventure vous tente, n'hésitez pas ;)
