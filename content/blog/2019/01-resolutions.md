+++
title = "Résolutions"
date = 2019-01-01T09:00:00+01:00
[taxonomies]
tags = ["Trucs"]
+++

Comme souvent le début d'année rime avec les résolutions que l'on prend et que l'on ne tient que trop peu. Bien évidemment je ne passe pas au travers de cela et voici ma liste.

  - Réduire voir arrêter la clope.
  - Reprendre la course à pied/VTT.
  - Faire un potager digne de ce nom. (ÉCHEC)
  - Avancer dans ma pile de lecture.
  - Réparer l'ampli et me remettre sérieusement à la guitare/piano.
  - Continuer à m'investir dans Solus.
  - Stabiliser ma méthode d'écriture et de prise de note.
  - Ne pas changer une énième fois de générateur pour le blog. (ÉCHEC)
  - Écrire plus régulièrement, sur des sujets divers et variés
