+++
title = "Blogus Musicus, Live report Steven Wilson"
date = 2019-01-23T09:00:00+01:00
[taxonomies]
tags = ["Musique", "Rock"]
[extra]
song = "[Porcupine Tree - Anesthetize](https://www.youtube.com/watch?v=MSEQZ8reJA4)"
+++

Parfois on revient à l'essentiel, on se prend de la musicalité dans la tronche et, il faut bien l'avouer ça fait un bien fou.

Lundi soir c'était donc en allant voir **Steven Wilson** que ce shoot légal et totalement bénéfique pour la santé fut pris, en famille avec le padre.

**Steven Wilson** c'est plus de trente ans de carrières pour un mec ayant 51 ans tout en paraissant 15 de moins. Des projets renommés comme [Porcupine 
Tree](https://fr.wikipedia.org/wiki/Porcupine_Tree), ou encore [Blackfield](https://fr.wikipedia.org/wiki/Blackfield) en tant que musicien, mais aussi en tant que producteur et mixeur pour trois albums d'[Opeth](https://fr.wikipedia.org/wiki/Opeth). À partir de 2009, via son propre label ( _Headphone Dust_ ), il sort son première album solo _Insurgentes_. S'en suit alors quatre albums dont le dernier, _To The Bone_, est sorti courant 2017.

C'est donc avec la tournée de ce dernier album que j'ai eu la chance de le voir au grand auditorium de la Cité des Congrès de Nantes.  
Plus de 2h de concert, avec des titres parcourants toute sa carrière, un show rodé, énergique, allant à l'essence même du rock. Le monsieur vit pour sa musique, 
interprète tous ses titres avec une passion rare. Il la transmet également par son, du pur rock en y allant franco sur sa guitare, la faisant vivre de 
par ces sons qu'elle dégage tout en restant d'une rigueur remarquable au niveau du son et de l'écoute dans le groupe.

Le reste de groupe n'est pas en reste non plus, on a d'ailleurs du mal à discerner qui est le plus doué. Un batteur monstrueux de technique au point où on ne lui 
prête plus trop attention tant sa précision et ses schémas rythmiques sont adaptés aux morceaux, un claviériste sortant des sons tout droit sortie des années où _Deep 
Purple_ et consorts étaient à l'affiche. Un guitariste au top, avec un jeu fluide et un sourire communicatif et un bassiste (qui se détache peut-être finalement) 
démontrant qu'il est possible de maitriser à la perfection le [Chapman Stick](https://fr.wikipedia.org/wiki/Chapman_Stick) tout en assurant également des lignes de 
basses absolument phénoménal. Et  comme tout bon bassiste, on a l'impression que cela est facile tant la désinvolture du musicien en impose sur scène.

Un live d'exception donc, travaillé, d'une justesse rare, éclectique à souhait et c'est ce dernier élément qui est le plus plaisant et qui révèle également tout le talent du gars. Pas de limitation à un _style_ de musique en particulier, mais plutôt un melting-pot d'influences diverses parfaitement assimilé et rendu en musique par un cinqua au sommet de son art. Un concert inoubliable tant sa musique m'accompagne depuis plus de dix ans (Merci papa, vraiment).

Pour les amateurs, la setlist du soir (seul bémol pour ma part je suis tombé sur le soir où _Arriving Somewhere But Not Here_ n'était pas joué.)

```
# Setlist   

## Set 1

  - Intro ("Truth" short film)
  - Nowhere Now
  - Pariah
  - Home Invasion
  - Regret #9
  - The Creator Has a Mastertape (Porcupine Tree song)
  - Refuge
  - The Same Asylum as Before
  - Ancestral

## Set 2

  - Cenotaph (Bass Communion song)
  - No Twilight Within the Courts of the Sun
  - Index
  - Permanating
  - Song of I
  - Lazarus (Porcupine Tree song)
  - Detonation
  - Song of Unborn
  - Vermillioncore
  - Sleep Together (Porcupine Tree song)


## Encore:

  - Blackfield (Blackfield song)
  - The Sound of Muzak (Porcupine Tree song)
  - The Raven That Refused to Sing
```
