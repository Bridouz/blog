+++
title = "Décroissance numérique, salvatrice simplification"
date = 2019-05-22T18:00:00+01:00
[taxonomies]
tags = ["Linux", "Vie 1.0"]
[extra]
song = "[Ben Harper - Jah Work](https://www.youtube.com/embed/lc2n58vzhz8)"
+++

L'informatique, le web, des univers fascinants, chronophages, attirants, repoussant même parfois. J'utilise ces technologies, comme beaucoup, pas mal de mon temps dans la journée. Lire des articles, faire défiler un flux sans-fin, Essayer des choses, trop de choses.

Depuis quelques mois, l'idée de restreindre cet usage me trotte dans la tête. Pouvoir retrouver le _réel_ plaisir de se prendre une heure, un jour, pour aller se balader sur le net et non suivre bêtement.

Une réflexion qui mature tranquillement depuis quelques mois et qui est enfin arrivée à terme il y a quelques jours. Le déclencheur était la dizaine d'onglets ouverts dans Firefox. À ce moment précis je me suis dit que ce n'était plus possible, qu'à trop en vouloir je me dispersais et que cela n'apportait rien de bien intéressant quand je faisais le point.

Frénétiquement je me suis détaché d'ailleurs de mes centres d'intérêts pendants quelque temps. Je ne lis plus autant qu'avant. Ma pile d'albums grossit et je ne prends plus le temps de pleinement écouter un album. Bref il est l'heure de la décroissance numérique.

Simplifier mon usage, mes pratiques, tout en gardant tout de même le côté bidouille amatrice qui me tient à cœur. Ce que je souhaite c'est un système qui tourne avec des outils simples.

Pour la distribution ce sera Void Linux, j'en avais déjà parler précédemment et fort est de constater que la distribution tourne toujours admirablement bien. La variante `musl` semble intéressante, cependant certains jeux ne sont pas compatibles et utilise trop `glibc` alors je vais prendre cette dernière.

  - **Cwm** comme gestionnaire de fenêtres, c'est simple et direct. La configuration est facile à prendre en main et le mélange clavier/sourie est vraiment bien équilibrée.
  - **Dmenu** pour accéder à tout ce que je peux/veux. Minimaliste et extensible à souhait avec des scripts.
  - Un navigateur sans onglet, **surf**, pour me recentrer sur ce que je lis, ce que je fais et non ce que je mets de côté.
  - Un éditeur de texte, **vim**. Puissant et léger à la fois.

La prochaine étape majeure s'annonce comme étant l'installation d'openBSD. Par malchance mon px portable a une carte wifi non supporté et le ventilo tournait à pleins régime non-stop lors d'une précédente installation. Ce sera donc lorsque mon fidèle acolyte lâchera que je me pencherai sur l'acquisition d'un pc portable compatible.

Côté serveur j'ai aussi fait le ménage. J'utilise un [Raspberry Pi 3](https://raspberrypi.org) pour héberger différents services.

  - Le blog et ses commentaires
  - Nextcloud
  - Un agrégateur de flux rss/atom
  - Une interface centralisant mes différents dépôts _git_

Du coup ce n'est pas grand-chose. Je n'exploite clairement pas toute la puissance de l'appareil alors, dans la mesure du possible, autant proposer simplement des pages html pures, peu gourmandes en ressources.
  
L'OS choisit est [Raspbian](https://raspbian.org/), c'est éprouvé, stable et officiellement supportée par la fondation et largement utilisé par les processeurs de Rpi. Ainsi le support est facile d'accès. Le serveur web est géré, quant à lui, par `nginx`. Là encore le logiciel est connu, stable et tourne sans le moindre soucis.

Le blog tourne avec [lgbt](https://ybad.name/Logiciel-libre/Code/lgbt.html), codé par l'ami bidouilleur [@prx](https://bsd.network/@prx). un script shell de ~300 lignes qui fait le job, couplé avec `git` le blog se met à jour comme un grand à chaque _push_. La génération se veut simple et minimaliste. C'est accessible et facilement extensible.

L'interface `git` est générée, toujours en simple page html, par [stagit](https://codemadness.org/stagit.html). Ayant plusieurs dépôts git voulant être accessibles j'utilise un script pour générer le tout. Ce dernier est lancer à chaque push git sur l'un des dépôts. Peut-être un peu extrême mais ça fait le job.

Ma lecture de flux rss subit le même traitement avec un logiciel, du même auteur, nommé [sfeeds](https://codemadness.org/sfeed-simple-feed-parser.html). Un fichier de configuration recueille les différents noms et urls des flux puis une commande transforme le tout en pages html, une autre créée un fichier txt qui sera utiliser sur le pc avec dmenu pour naviguer depuis le bureau.  
Ces différentes actions sont centralisées dans un script shell se lançant, via `cron` toutes les 8 heures.

Ralentissement donc et refonte de mon usage du web et de l'informatique. Pour le moment ça tient la route et j'ai l'impression que je suis plus à même de réaliser le pourquoi du changement. Une décroissance salvatrice, reconnectant mon esprit avec lui-même.
