+++
title = "Je ne connais plus mon nom"
date = 2019-05-06T10:00:00+00:00
[taxonomies]
tags = ["Aidesoignant", "Vie 1.0"]
+++

> Billet faisant partie d'un diptyque avec [Je suis là pour toi](@/blog/2019/10-jesuislapourtoi.md).

Cerveau off/on, j'ouvre les yeux. Incapable de savoir l'heure qu'il est. La pièce me paraît familière, mais je ne pourrais dire où je suis. Je  regarde autour de moi, il y a une porte. Je l'ouvre et je sors dans le couloir. Je marche, me saisis d'objets à l'abandon sur une table. J'arrive dans une grande salle, au loin je distingue un garçon habillé en rose avec, mon coeur se met à battre la chamade, de la nourriture.

Il me regarde, me sourit, me dit quelque-chose en levant sa main face à moi. Il commence à brasser de la nourriture, je le regarde, avec envie, ça a l'air bon tout cela. Oh ! Il m'apporte une assiette. Merci lui dis-je en avalant mon jus d'orange à toute vitesse.

L'assiette finit je relève la tête, où suis-je ? Je me lève et je commence à marcher. La porte a l'air fermé, je repars dans l'autre sens, je rentre dans des pièces où des gens sont allongés dans un lit, faisant un bruit de gens qui dorment. Je rentre dans une autre chambre, la personne allongée dans ce lit ne dort pas, elle crie. Mais pourquoi crie-t-elle comme cela, j'essaie d'en faire abstraction et je continue de farfouiller ce meuble rempli de trésor.  
Un garçon en rose rentre alors dans la chambre, peut-être que lui aussi il en a marre d'entendre ce monsieur crier. Il se tourne vers moi, me parle. Une autre langue probablement. Il me passe le bras autour des épaules et m'invite à le suivre en dehors de la chambre. Pas sympa ça, je râle un peu et je le suis.

Où suis-je ? Le garçon m’emmène dans un dédale, je le suis, lui prend la main pour me rassurer. Dans la pièce où il m'emmène il y a quelque-chose de familier, avec un trou et de l'eau en son centre. J'inspecte les lieux, me saisit d'un petit rectangle assez doux, le monsieur est toujours là, sa bouche bouge mais impossible de comprendre ce qu'il dit, à qui parle il. Moi ? Je lui réponds quelque-chose, on verra bien.

Tout en parlant, l'homme commence à enlever mes vêtements, des images me reviennent, je n'aime pas ça. Alors je dis "Non !" puis "NON ! STOP !". L'homme arrête, me prend la main délicatement, me regarde dans les yeux, fredonne quelque-chose. Oh de la musique, j'aime cela moi.

Bon, je n'aime pas être  à moitié nu devant quelqu'un, ce dernier me touchant avec de l'eau et du savon. Une fois habillé c'est déjà mieux.

Nous ressortons de cette pièce, je me saisis de quelques biblos laissés ici et là, le monsieur m'invite à manger un petit truc tout rond, couleur chocolat au lait. Impossible de savoir ce que c'est, mais que c'est bon.

Où suis-je ? Cette pièce me parait familière, il y a pas mal de personnes ici, je marche, les regarde, je dois marcher car j'ai du travail. Oh ! Du pain posé sur le bar, et hop ça c'est pour mon estomac. Je marche, encore et toujours, je dois marcher, je le sens.

Les personnes dans la salle sont maintenant toutes assises et, l'homme en rose, il commence à me faire des signes et me montre un objet semblable à ceux qu'ont les autres personnes sous leurs fesses. Je m'approche, m’assieds à mon tour. Il me donne une serviette, je croque dedans, ça ne vient pas.  
Il installe un grand rectangle rouge devant moi, un verre et des machins couleurs métal que j'utiliserai pour manger. Mais entre nous je n'ai jamais compris comment cela fonctionnait alors j'utilise mes mains, plus rapide, plus facile.

Ah ! Que je n'aime pas être assis, il semblerait d'ailleurs que je ne puisse pas sortir, le fauteuil ne bouge pas. J'essaie encore une fois et si ça ne marche pas je mangerai ce qu'il y a dans mon assiette.

[...]

Je me réveille brutalement, où suis-je ? j'essaie de me lever, réflexe. Je commence à marcher, je ramasse des objets laissés à l'abandon sur la table proche de moi. Un bloc de feuilles assemblées les unes aux autres, des serviettes, un de ces machins en métal. J'essaie de porter cela à ma bouche. C'est dur comme du fer, impossible à manger.

Je marche, encore et toujours, je pénètre dans une pièce. Il y a beaucoup de personnes en roses, ça parle, ça rigole, ça vit. Je fais le tour de la pièce, je repère un homme, en rose lui aussi, je m'approche vers lui, résiste à la tentation de prendre cette tasse qui m'appelle sur le trajet, je luis pose ma main sur son épaule et dit :

> " Ouais... Oh ! Tu vois là, y'a pleins de tibikodu plabsdatroptruc."

L'homme semble les voir lui aussi, il suit mon doigt du regard et me montre également la même-chose. Ouf, je suis rassuré, je pensais que j'étais le seul à les voir.

L'homme se lève, me tent la main, je lui serre. Il me regarde dans les yeux, avec un sourire qui fait plaisir à voir. Me souhaite un bon après-midi et me dit à demain.

Qui est-ce ? Pourquoi demain ? Et moi d'ailleurs, comment je m'appelle, qu'est-ce que je fous là. Je commence à sangloter, pleurer, lui serrer la main de plus en plus fort, pour me raccrocher à la vie, au présent.

L'homme commence à avoir les yeux humides, il me prend dans ses bras, me masse les épaules, prononce un prénom qui me fait croiser son regard. Il me semble que c'est le mien, ou peut-être pas. Je me saisis d'un bout de pain traînant sur la table et je m'en vais. Je marche, encore, toujours, pour vivre, pour survivre, seul, dans un monde inconnu où, tout de même, des phares sont là pour me guider.
