+++
title = "Le soin, outil pour rétablir l'humain"
date = 2017-06-13T18:00:00+01:00
[taxonomies]
tags = ["Aidesoignant", "Vie 1.0"]
+++

Depuis quelques jours je suis passé sur un nouveau poste, dans un nouveau service. Après 7 longs mois sur un poste bouche-trou à ranger des compotes et me balader dans le CHU pour livrer des bilans sanguins et remonter des médocs je suis enfin retourné dans mon _cœur de métier_ (mot-compte-triple au CHU en ce moment): **le soin**.

 Direction l'unité neuro-vasculaire aka l'unité post-AVC aka l'unité de la transpiration, de l'urgence bordélique et des heures sup'. Direction le boulot, ce pour quoi je me plais à me lever le matin et à finir tard le soir.
 Au sein du service la majeure partie des patients ont été victimes d'un AVC, plus ou moins grave, mais en règle générale ça a laissé pas mal de traces, surtout des paralysies d'une moitié du corps _(hémiplégie)_ et/ou une perte de la parole _(aphasie)_.

C'est dans des services comme celui-ci que le métier d'aide-soignant prend tout son sens. Il reste un métier physique où devoir laver des gens ne contrôlant pas leur corps rime avec effort pour la mobilisation, grosse sueur pendant les mises au fauteuil et couchés, mais il s'enrichit également de petites victoires qui font que le métier ne se résume pas seulement à l'_entretien_ d'un corps.

Voir des personnes ayant perdu l'usage de la parole est une chose à laquelle on s'habitue, qu'on apprend à comprendre pour au final développé un moyen propre à chaque patient pour communiquer, quand cela est possible. Avec cette communication alternative le soin continue et l'équipe au complet est derrière le patient pour qu'il retrouve l'usage de la parole. Le cerveau est une structure plus que complexe, si bien que chaque AVC est singulier.  
Il y a bien-sûr des effets communs à certains types d'AVC, mais chaque être humain aura également des effets propres. Un AVC fatigue énormément le corp ainsi que le moral il faut alors veiller à prévenir cette fatigue et stimuler la personne pour essayer de la remettre sur le chemin de l'activité.

L'un des effets les plus communs après un AVC reste la perte de la parole pouvant également venir avec une perte du langage (compréhension, expression). Il faut donc arriver à créer une connexion entre le patient et le soignant pour établir une communication humaine allant dans le sens du soin.

Que ce soit par sa posture, ses gestes ou encore l'intonation de sa voix le soignant joue un rôle essentiel dans la reconstruction de la personne victime d'un AVC, elle sert de guide pour rétablir une faculté endommagée.  
On se retrouve alors dans une position similaire à celles que peuvent vivre de jeunes parents entendant pour la première fois leur bambin prononcer un mot. On s’émerveille d'un petit rien, qui en réalité signifie beaucoup pour le patient.

Le soin se présente sous diverses formes, allant d'une aide partielle à une prise en charge complète, d'attentions affectueuses envers des corps et esprits endommagés. Raser quelqu'un prend du temps (surtout quand soit même on se laisse pousser la barbe), demande une vigilance accrue pour ne pas faire souffrir la personne mais au final, le sourire d'une personne fraîchement rasée, retrouvant une part de lui qui avait disparu avec l'AVC est un plaisir total.

Le métier reste mal-connu, probablement dévalorisant apparemment selon certains, mais il y a de quoi faire dans une carrière sans jamais s'ennuyer, il y a du taff partout et même plusieurs taffs par personne tant la pénurie commence à se faire sentir. S'occuper des humains, j'irai même jusqu'à être russophile et parler de _camarades_, est un plaisir quotidien qui permet d'enrichir sa vision du monde.  
Un AVC est un épisode douloureux dans une vie et il se doit donc d'être pris en charge de la meilleure façon qu'il soit, dans sa globalité. Ne pas oublier que derrière le terme médical il y a une personne qui a souffert, qui va souffrir eventuellement par la suite de cette perte d'autonomie.
